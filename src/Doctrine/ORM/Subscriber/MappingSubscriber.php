<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Pressop\Component\Translatable\Exception\DoubleInterfaceException;
use Pressop\Component\Translatable\Exception\MissingTranslationPrefixException;
use Pressop\Component\Translatable\Model\TranslatableInterface;
use Pressop\Component\Translatable\Model\TranslationInterface;

/**
 * Class MappingSubscriber
 *
 * @author Benjamin Georgeault
 */
class MappingSubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        $both = 0;
        if ($reflectionClass->implementsInterface(TranslatableInterface::class)) {
            $this->mappingTranslatable($metadata, $reflectionClass);
            $both++;
        }

        if ($reflectionClass->implementsInterface(TranslationInterface::class)) {
            if (!preg_match('/\\\\Translation\\\\([^\\\\]+)$/', $reflectionClass->getName())) {
                throw new MissingTranslationPrefixException($reflectionClass->getName());
            }

            $this->mappingTranslation($metadata, $reflectionClass);
            $both++;
        }

        if ($both > 1) {
            throw new DoubleInterfaceException($reflectionClass->getName());
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @param \ReflectionClass $reflectionClass
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    private function mappingTranslatable(ClassMetadata $metadata, \ReflectionClass $reflectionClass)
    {
        if (!$metadata->hasAssociation('translations')) {
            $metadata->mapOneToMany([
                'fieldName' => 'translations',
                'targetEntity' => $reflectionClass->getNamespaceName() . '\\Translation\\' . $reflectionClass->getShortName(),
                'mappedBy' => 'translatable',
                'indexBy' => 'locale',
                'cascade' => ['persist', 'merge', 'remove'],
                'fetch' => ClassMetadataInfo::FETCH_EAGER,
                'orphanRemoval' => true,
            ]);
        }

        if (!$metadata->hasField('fallback')) {
            $metadata->mapField([
                'fieldName' => 'fallback',
                'type' => 'string',
                'length' => 5,
            ]);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @param \ReflectionClass $reflectionClass
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    private function mappingTranslation(ClassMetadata $metadata, \ReflectionClass $reflectionClass)
    {
        if (!$metadata->hasField('locale')) {
            $metadata->mapField([
                'fieldName' => 'locale',
                'type' => 'string',
                'length' => 5,
            ]);
        }

        if (!$metadata->hasAssociation('translatable')) {
            $metadata->mapManyToOne([
                'fieldName' => 'translatable',
                'targetEntity' => preg_replace('/\\\\Translation\\\\([^\\\\]+)$/', '\\\\\1', $reflectionClass->getName()),
                'inversedBy' => 'translations',
                'cascade' => ['persist', 'merge'],
                'fetch' => ClassMetadataInfo::FETCH_EAGER,
                'joinColumns' => [[
                    'name' => 'translatable_id',
                    'referencedColumnName' => 'id',
                    'onDelete' => 'CASCADE',
                    'nullable' => false,
                ]],
            ]);
        }

        $metadata->table['uniqueConstraints'][$metadata->getTableName().'_translation_unique'] = [
            'columns' => ['translatable_id', 'locale'],
        ];
    }
}
