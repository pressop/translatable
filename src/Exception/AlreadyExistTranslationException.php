<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Exception;

use Pressop\Component\Translatable\Model\TranslatableInterface;

/**
 * Class AlreadyExistTranslationException
 *
 * @author Benjamin Georgeault
 */
class AlreadyExistTranslationException extends \RuntimeException
{
    /**
     * @var TranslatableInterface
     */
    private $translatable;

    /**
     * @var string
     */
    private $locale;

    /**
     * NotFoundTranslationException constructor.
     * @param TranslatableInterface $translatable
     * @param string $locale
     */
    public function __construct(TranslatableInterface $translatable, string $locale)
    {
        $this->translatable = $translatable;
        $this->locale = $locale;

        parent::__construct(sprintf('Translation for locale "%s" already exist.', $locale));
    }
}
