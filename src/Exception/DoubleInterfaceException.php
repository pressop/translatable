<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Exception;

use Pressop\Component\Translatable\Model\TranslatableInterface;
use Pressop\Component\Translatable\Model\TranslationInterface;

/**
 * Class DoubleInterfaceException
 *
 * @author Benjamin Georgeault
 */
class DoubleInterfaceException extends \LogicException
{
    /**
     * DoubleInterfaceException constructor.
     * @param string $class
     */
    public function __construct(string $class)
    {
        parent::__construct(sprintf(
            'The class "%s" cannot implement both "%s" and "%s" interfaces. Choose only one.',
            $class,
            TranslationInterface::class,
            TranslatableInterface::class
        ));
    }
}
