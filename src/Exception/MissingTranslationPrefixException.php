<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Exception;

/**
 * Class MissingTranslationPrefixException
 *
 * @author Benjamin Georgeault
 */
class MissingTranslationPrefixException extends \LogicException
{
    /**
     * @inheritDoc
     */
    public function __construct(string $class)
    {
        parent::__construct(sprintf('The class "%s" missing Translation prefix.', $class));
    }
}
