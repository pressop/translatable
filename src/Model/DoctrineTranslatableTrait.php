<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Pressop\Component\Translatable\Exception\AlreadyExistTranslationException;
use Pressop\Component\Translatable\Exception\NotFoundTranslationException;

/**
 * Trait DoctrineTranslatableTrait
 *
 * For usage with
 *
 * @author Benjamin Georgeault
 * @see \Pressop\Component\Translatable\Model\TranslatableInterface
 */
trait DoctrineTranslatableTrait // implements TranslatableInterface
{
    /**
     * @var TranslationInterface[]|Collection
     */
    protected $translations;

    /**
     * @return TranslationInterface[]|Collection
     */
    public function getTranslations()
    {
        return $this->translations ? : $this->translations = new ArrayCollection();
    }

    /**
     * @param string $locale
     * @param string|null $fallback
     * @return TranslationInterface
     * @throws NotFoundTranslationException
     */
    public function getTranslation(string $locale, string $fallback = null): TranslationInterface
    {
        if ($this->hasTranslation($locale)) {
            return $this->getTranslations()->get($locale);
        }

        if (null !== $fallback && $this->hasTranslation($fallback)) {
            return $this->getTranslations()->get($fallback);
        }

        throw new NotFoundTranslationException($this, $locale);
    }

    /**
     * @param string $locale
     * @return bool
     */
    public function hasTranslation(string $locale): bool
    {
        return $this->getTranslations()->containsKey($locale);
    }

    /**
     * @param TranslationInterface $translation
     * @return $this
     * @throws AlreadyExistTranslationException
     */
    public function addTranslation(TranslationInterface $translation)
    {
        if ($this->hasTranslation($locale = $translation->getLocale())) {
            throw new AlreadyExistTranslationException($this, $locale);
        }

        $translation->setTranslatable($this);
        $this->getTranslations()->set($locale, $translation);

        return $this;
    }
}
