<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Model;

use Pressop\Component\Translatable\Exception\AlreadyExistTranslationException;
use Pressop\Component\Translatable\Exception\NotFoundTranslationException;

/**
 * Object that has to be translatable has to implement this interface.
 *
 * @author Benjamin Georgeault
 */
interface TranslatableInterface
{
    /**
     * Get all Translations for this Translatable.
     *
     * @return TranslationInterface[]
     */
    public function getTranslations();

    /**
     * Get a Translation by its locale.
     *
     * @param string $locale
     * @param null|string $fallback
     * @return TranslationInterface
     * @throws NotFoundTranslationException
     */
    public function getTranslation(string $locale, string $fallback = null): TranslationInterface;

    /**
     * @param string $locale
     * @param string|null $fallback
     * @return null|TranslationInterface
     */
    public function getTranslationOrNull(string $locale, string $fallback = null): ?TranslationInterface;

    /**
     * Check if the given locale exist for this Translatable.
     *
     * @param string $locale
     * @return bool
     */
    public function hasTranslation(string $locale): bool;

    /**
     * Add a Translation.
     *
     * @param TranslationInterface $translation
     * @return $this
     * @throws AlreadyExistTranslationException
     */
    public function addTranslation(TranslationInterface $translation);

    /**
     * @return null|string
     */
    public function getFallback(): ?string;

    /**
     * @param string $fallback
     * @return $this
     */
    public function setFallback(string $fallback);
}
