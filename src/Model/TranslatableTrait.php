<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Model;

use Pressop\Component\Translatable\Exception\AlreadyExistTranslationException;
use Pressop\Component\Translatable\Exception\NotFoundTranslationException;

/**
 * Trait TranslatableTrait
 *
 * @author Benjamin Georgeault
 * @see \Pressop\Component\Translatable\Model\TranslatableInterface
 */
trait TranslatableTrait // implements TranslatableInterface
{
    /**
     * @var TranslationInterface[]
     */
    protected $translations;

    /**
     * @var string|null
     */
    protected $fallback;

    /**
     * @return TranslationInterface[]
     */
    public function getTranslations()
    {
        return $this->translations ? : $this->translations = [];
    }

    /**
     * @param string $locale
     * @param string|null $fallback
     * @return TranslationInterface
     * @throws NotFoundTranslationException
     */
    public function getTranslation(string $locale, string $fallback = null): TranslationInterface
    {
        if ($this->hasTranslation($locale)) {
            return $this->getTranslations()[$locale];
        }

        if (null === $fallback) {
            $fallback = $this->getFallback();
        }

        if (null !== $fallback && $this->hasTranslation($fallback)) {
            return $this->getTranslations()[$fallback];
        }

        throw new NotFoundTranslationException($this, $locale);
    }

    /**
     * @param string $locale
     * @param string|null $fallback
     * @return null|TranslationInterface
     */
    public function getTranslationOrNull(string $locale, string $fallback = null): ?TranslationInterface
    {
        try {
            return $this->getTranslation($locale, $fallback);
        } catch (NotFoundTranslationException $e) {
            return null;
        }
    }

    /**
     * @param string $locale
     * @return bool
     */
    public function hasTranslation(string $locale): bool
    {
        return array_key_exists($locale, $this->getTranslations());
    }

    /**
     * @param TranslationInterface $translation
     * @return $this
     * @throws AlreadyExistTranslationException
     */
    public function addTranslation(TranslationInterface $translation)
    {
        if ($this->hasTranslation($locale = $translation->getLocale())) {
            throw new AlreadyExistTranslationException($this, $locale);
        }

        $translation->setTranslatable($this);
        $this->getTranslations();
        $this->translations[$locale] = $translation;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFallback(): ?string
    {
        return $this->fallback;
    }

    /**
     * @param string $fallback
     * @return $this
     */
    public function setFallback(string $fallback)
    {
        $this->fallback = $fallback;

        return $this;
    }
}
