<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Model;

/**
 * Interface TranslationInterface
 *
 * @author Benjamin Georgeault
 */
interface TranslationInterface
{
    /**
     * @return string
     */
    public function getLocale(): string;

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale(string $locale);

    /**
     * @return TranslatableInterface
     */
    public function getTranslatable(): TranslatableInterface;

    /**
     * @param TranslatableInterface $translatable
     * @return $this
     */
    public function setTranslatable(TranslatableInterface $translatable);
}
