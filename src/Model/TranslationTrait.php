<?php

/*
 * This file is part of the pressop/translation package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Translatable\Model;

/**
 * Trait TranslationTrait
 *
 * @author Benjamin Georgeault
 * @see TranslationInterface
 */
trait TranslationTrait // implements TranslationInterface
{
    /**
     * @var string
     */
    protected $locale = 'en';

    /**
     * @var TranslatableInterface
     */
    protected $translatable;

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return TranslatableInterface
     */
    public function getTranslatable(): TranslatableInterface
    {
        return $this->translatable;
    }

    /**
     * @param TranslatableInterface $translatable
     * @return $this
     */
    public function setTranslatable(TranslatableInterface $translatable)
    {
        $this->translatable = $translatable;

        return $this;
    }
}
